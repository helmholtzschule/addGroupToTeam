module gitlab.com/helmholtzschule/addGroupToTeam

go 1.16

require (
	github.com/iancoleman/strcase v0.2.0
	github.com/kataras/golog v0.1.7
	github.com/pkg/errors v0.9.1
	github.com/robfig/cron/v3 v3.0.1
	github.com/spf13/viper v1.8.1
	github.com/stretchr/testify v1.7.0
	golang.org/x/oauth2 v0.0.0-20210402161424-2e8d93401602
)
