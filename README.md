# AddGroupToTeam

Hinzufügen aller Mitglieder einer Gruppe zu einem Team.

## Benutzung
1. App-Registrierung in Azure
    * Benötigte Berechtigungen (application):
        * GroupMember.Read.All
        * TeamMember.ReadWrite.All
    * Client-Secret erstellen
2. Abschnitt `msal` in der Konfigurationsdatei (`add-group-to-team.conf.yml`) anpassen
3. Ausführen
    * Unix: addGroupToTeam $TEAM_ID $GROUP_ID
    * Windows: addGroupToTeam.exe $TEAM_ID $GROUP_ID
    
**WICHTIG**: Der Programmaufruf muss in dem Ordner geschehen, 
in welchem sich auch die Konfigurationsdatei befinden. 
