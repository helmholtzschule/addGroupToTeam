/*
 * Copyright (c) 2021 Julian Hackenberg. All rights reserved.
 */

package msal

import (
	"context"
	"net/http"

	"github.com/spf13/viper"
)

// NewClient creates a new http client which automatically adds the access token as Authorization header
func NewClient(ctx context.Context, conf *viper.Viper) *http.Client {
	return oauth2Config(conf).Client(ctx)
}
