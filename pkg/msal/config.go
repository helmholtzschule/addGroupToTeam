/*
 * Copyright (c) 2021 Julian Hackenberg. All rights reserved.
 */

package msal

import (
	"github.com/spf13/viper"
	"golang.org/x/oauth2/clientcredentials"
	"golang.org/x/oauth2/microsoft"
)

func oauth2Config(conf *viper.Viper) *clientcredentials.Config {
	return &clientcredentials.Config{
		ClientID:     conf.GetString("ClientID"),
		ClientSecret: conf.GetString("ClientSecret"),
		TokenURL:     microsoft.AzureADEndpoint(conf.GetString("TenantID")).TokenURL,
		Scopes:       conf.GetStringSlice("Scopes"),
	}
}
