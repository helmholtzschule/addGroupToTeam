/*
 * Copyright (c) 2021 Julian Hackenberg. All rights reserved.
 */

package msgraph

import (
	"encoding/json"
	"fmt"
	"io"
	"reflect"
)

// DecodeDefaultArrayResponse decodes a default response where the value is an array.
// Multiple pages are combined and decoded to a single value.
// v must be a slice pointer.
func DecodeDefaultArrayResponse(readers []io.Reader, v interface{}, key string) error {
	return unmarshalDefaultArrayResponse(len(readers), func(idx int, v interface{}) error {
		return DecodeDefaultResponse(readers[idx], v, key)
	}, v)
}

// UnmarshalDefaultArrayResponse unmarshalls a default response where the value is an array.
// Multiple pages are combined and decoded to a single value.
// v must be a slice pointer.
func UnmarshalDefaultArrayResponse(data [][]byte, v interface{}, key string) error {
	return unmarshalDefaultArrayResponse(len(data), func(idx int, v interface{}) error {
		return UnmarshalDefaultResponse(data[idx], v, key)
	}, v)
}

func unmarshalDefaultArrayResponse(count int, unmarshaler func(idx int, v interface{}) error, v interface{}) error {
	valueElem := reflect.ValueOf(v).Elem()
	for i := 0; i < count; i++ {
		arr := reflect.New(reflect.TypeOf(v))
		if err := unmarshaler(i, arr.Interface()); err != nil {
			return UnmarshalError{err: err}
		}
		valueElem.Set(reflect.AppendSlice(valueElem, arr.Elem().Elem()))
	}

	return nil
}

func DecodeDefaultResponse(r io.Reader, v interface{}, key string) error {
	return unmarshalDefaultResponse(func(defaultResponse interface{}) error {
		return json.NewDecoder(r).Decode(defaultResponse)
	}, v, key)
}

func UnmarshalDefaultResponse(data []byte, v interface{}, key string) error {
	return unmarshalDefaultResponse(func(defaultResponse interface{}) error {
		return json.Unmarshal(data, defaultResponse)
	}, v, key)
}

func unmarshalDefaultResponse(unmarshaler func(defaultResponse interface{}) error, v interface{}, key string) error {
	defaultResponse := NewDefaultResponse(v, key)
	if err := unmarshaler(defaultResponse); err != nil {
		return UnmarshalError{err: err}
	}

	reflect.ValueOf(v).Elem().Set(reflect.ValueOf(GetDataFromDefaultResponse(defaultResponse)).Elem())

	return nil
}

func NewDefaultResponse(v interface{}, key string) interface{} {
	return reflect.New(reflect.StructOf([]reflect.StructField{{
		Name: "Data",
		Type: reflect.TypeOf(v),
		Tag:  reflect.StructTag(fmt.Sprintf(`json:"%s"`, key)),
	}})).Interface()
}

func GetDataFromDefaultResponse(response interface{}) interface{} {
	return reflect.ValueOf(response).Elem().FieldByName("Data").Interface()
}

type UnmarshalError struct {
	err error
}

func (e UnmarshalError) Error() string {
	return fmt.Sprintf("error while unmarshalling: %s", e.err)
}
