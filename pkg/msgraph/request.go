/*
 * Copyright (c) 2021 Julian Hackenberg. All rights reserved.
 */

package msgraph

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

type batchRequest struct {
	ID      string            `json:"id"`
	Method  string            `json:"method"`
	URL     string            `json:"url"`
	Body    json.RawMessage   `json:"body"`
	Headers map[string]string `json:"headers"`
}

func newBatchRequests(requests map[string]*Request) ([]*batchRequest, error) {
	var ret []*batchRequest
	for id, request := range requests {
		req, err := newBatchRequest(id, request)
		if err != nil {
			return nil, err
		}
		ret = append(ret, req)
	}

	return ret, nil
}

func newBatchRequest(id string, request *Request) (*batchRequest, error) {
	r := &batchRequest{
		ID:      id,
		Method:  request.Method,
		URL:     request.path(),
		Headers: map[string]string{},
	}

	if request.Body != nil {
		b, err := ioutil.ReadAll(request.Body)
		if err != nil {
			return nil, BatchRequestError{
				id:      id,
				request: request,
				err:     fmt.Errorf("error while reading request body: %s", err),
			}
		}
		r.Body = b
	}
	for header := range request.Headers {
		r.Headers[header] = request.Headers.Get(header)
	}

	return r, nil
}

type Request struct {
	Method   string
	Resource string
	Filter   string
	Select   []string
	// may be omitted
	Query url.Values
	// may be omitted
	Body io.Reader
	// may be omitted, only strictly needed in batch requests
	Headers http.Header
}

func (request *Request) httpRequest() (*http.Request, error) {
	req, err := http.NewRequest(
		request.Method,
		"https://graph.microsoft.com/v1.0"+request.path(),
		request.Body,
	)
	if err != nil {
		return nil, RequestCreationError{err}
	}
	for header := range request.Headers {
		req.Header.Set(header, request.Headers.Get(header))
	}

	return req, err
}

func (request *Request) path() string {
	if request.Query == nil {
		request.Query = url.Values{}
	}
	if request.Select != nil {
		request.Query.Set("$select", strings.Join(request.Select, ","))
	}
	if request.Filter != "" {
		request.Query.Set("$filter", request.Filter)
	}

	return fmt.Sprintf("/%s%s%s", request.Resource, func() string {
		if len(request.Query) == 0 {
			return ""
		}

		return "?"
	}(), request.Query.Encode())
}

type BatchRequestError struct {
	id      string
	request *Request
	err     error
}

func (e BatchRequestError) Error() string {
	return fmt.Sprintf("error while creating ms graph batch request at request %q (Method: %q, Rescource: %q)", e.id, e.request.Method, e.request.Resource)
}

type RequestCreationError struct {
	err error
}

func (e RequestCreationError) Error() string {
	return fmt.Sprintf("error while creating ms graph request: %s", e.err)
}
