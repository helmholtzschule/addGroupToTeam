/*
 * Copyright (c) 2021 Julian Hackenberg. All rights reserved.
 */

package msgraph

import "net/http"

type Client struct {
	msalClient *http.Client
}

// NewClient creates a new Client instance
func NewClient(msalClient *http.Client) *Client {
	return &Client{msalClient: msalClient}
}
