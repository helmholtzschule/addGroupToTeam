/*
 * Copyright (c) 2021 Julian Hackenberg. All rights reserved.
 */

package msgraph

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
)

type BatchResponse struct {
	ID       string
	Err      error
	Response *http.Response
}

func (response *BatchResponse) UnmarshalJSON(b []byte) error {
	var info struct {
		ID         string            `json:"id"`
		StatusCode int               `json:"status"`
		Headers    map[string]string `json:"headers"`
		Body       json.RawMessage   `json:"body"`
	}
	if err := json.Unmarshal(b, &info); err != nil {
		return err
	}
	response.ID = info.ID
	if err := checkForError(info.StatusCode, bytes.NewBuffer(info.Body)); err != nil {
		response.Err = err
		return nil
	}
	response.Response = &http.Response{
		Status:        http.StatusText(info.StatusCode),
		StatusCode:    info.StatusCode,
		Header:        http.Header{},
		Body:          io.NopCloser(bytes.NewBuffer(info.Body)),
		ContentLength: int64(len(info.Body)),
	}
	for header, value := range info.Headers {
		response.Response.Header.Set(header, value)
	}

	return nil
}
