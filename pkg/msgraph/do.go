/*
 * Copyright (c) 2021 Julian Hackenberg. All rights reserved.
 */

package msgraph

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
)

func (client *Client) DoBatch(requests ...*Request) ([]*BatchResponse, error) {
	requestMap := map[string]*Request{}
	for i, request := range requests {
		requestMap[strconv.Itoa(i)] = request
	}
	responseMap, err := client.DoBatchWithIDs(requestMap)
	if err != nil {
		return nil, err
	}
	responses := make([]*BatchResponse, len(requests))
	for id, response := range responseMap {
		idx, err := strconv.Atoi(id)
		if err != nil {
			return nil, RequestExecutionError{err: err}
		}
		responses[idx] = response
	}

	return responses, nil
}

func (client *Client) DoBatchWithIDs(requests map[string]*Request) (map[string]*BatchResponse, error) {
	body := struct {
		Requests []*batchRequest `json:"requests"`
	}{}
	r, err := newBatchRequests(requests)
	if err != nil {
		return nil, RequestExecutionError{err: err}
	}
	body.Requests = r

	bodyBytes, err := json.Marshal(body)
	if err != nil {
		return nil, RequestExecutionError{err: err}
	}
	responses, err := client.Do(&Request{
		Method:   http.MethodPost,
		Resource: "$batch",
		Headers: map[string][]string{
			"Content-Type": {"application/json"},
		},
		Body: bytes.NewBuffer(bodyBytes),
	})
	if err != nil {
		return nil, err
	}
	ret := map[string]*BatchResponse{}
	for _, resp := range responses {
		var batchResponses []*BatchResponse
		if err := DecodeDefaultResponse(resp.Body, &batchResponses, "responses"); err != nil {
			return nil, RequestExecutionError{err: err}
		}
		for _, r := range batchResponses {
			ret[r.ID] = r
		}
		_ = resp.Body.Close()
	}

	return ret, nil
}

func (client *Client) DoDefaultArrayRequest(request *Request, v interface{}, key string) error {
	responses, err := client.Do(request)
	if err != nil {
		return err
	}
	readers := make([]io.Reader, len(responses))
	for i, resp := range responses {
		readers[i] = resp.Body
	}
	if err := DecodeDefaultArrayResponse(readers, v, key); err != nil {
		return err
	}
	for _, response := range responses {
		_ = response.Body.Close()
	}

	return nil
}

// Do executes the request
func (client *Client) Do(request *Request) ([]*http.Response, error) {
	var responses []*http.Response
	nextRequest, err := request.httpRequest()
	if err != nil {
		return nil, RequestExecutionError{err: err}
	}
	for nextRequest != nil {
		resp, err := client.msalClient.Do(nextRequest)
		if err != nil {
			return nil, RequestExecutionError{err: err}
		}

		if err := checkHttpResponseForError(resp); err != nil {
			_ = resp.Body.Close()
			return nil, RequestExecutionError{err: err}
		}

		info := struct {
			Next *string `json:"@odata.nextLink"`
		}{}
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, RequestExecutionError{err: err}
		}
		_ = resp.Body.Close()
		if err := json.Unmarshal(bodyBytes, &info); err != nil {
			return nil, RequestExecutionError{err: err}
		}
		resp.Body = io.NopCloser(bytes.NewBuffer(bodyBytes))
		nextRequest = nil
		if info.Next != nil {
			nextRequest, err = http.NewRequest(http.MethodGet, *info.Next, nil)
			if err != nil {
				return nil, RequestExecutionError{err: err}
			}
		}
		responses = append(responses, resp)
	}

	return responses, nil
}

func checkHttpResponseForError(resp *http.Response) error {
	if err := checkForError(resp.StatusCode, resp.Body); err != nil {
		defer func() { _ = resp.Body.Close() }()
		return err
	}

	return nil
}

func checkForError(statusCode int, body io.Reader) error {
	if statusCode < 200 || statusCode >= 300 {
		if body != http.NoBody {
			var e GraphError
			_ = DecodeDefaultResponse(body, &e, "error")
			return e
		}
		return errors.New("got an error response, but no further information could be retrieved")
	}
	return nil
}

type RequestExecutionError struct {
	err error
}

// Error implements the error interface
func (e RequestExecutionError) Error() string {
	return fmt.Sprintf("error while executing ms graph request: %s", e.err)
}
