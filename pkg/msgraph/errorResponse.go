/*
 * Copyright (c) 2021 Julian Hackenberg. All rights reserved.
 */

package msgraph

import (
	"fmt"
)

type GraphError struct {
	Code       string `json:"code"`
	Message    string `json:"message"`
	InnerError struct {
		RequestID string `json:"requestId"`
		Date      string `json:"date"`
	} `json:"innerError"`
}

func (e GraphError) Error() string {
	return fmt.Sprintf("ms graph error response: code: %q, message: %q", e.Code, e.Message)
}
