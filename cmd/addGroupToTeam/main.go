/*
 * Copyright (c) 2021 Julian Hackenberg. All rights reserved.
 */

package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"github.com/pkg/errors"
	"gitlab.com/helmholtzschule/addGroupToTeam/internal/app/addGroupToTeam/application"
	"gitlab.com/helmholtzschule/addGroupToTeam/pkg/msgraph"
)

var app = application.NewApp()

func main() {
	teamID, groupID := os.Args[1], os.Args[2]
	app.Logger.Debugf("adding members of group %q to team %q", groupID, teamID)
	members, err := fetchGroupMembers(groupID)
	if err != nil {
		app.Logger.Fatal(errors.Wrapf(err, "fetch group members of %q", groupID))
	}
	for _, member := range members {
		app.Logger.Debugf("found user %s", member)
	}
	for i := 0; i < len(members); i += 5 {
		last := i + 5
		if last > len(members) {
			last = len(members)
		}
		if err := AddUsersToTeam(teamID, members[i:last]); err != nil {
			app.Logger.Fatal(errors.Wrapf(err, "add users to team %q", teamID))
		}
	}
}

func AddUsersToTeam(teamID string, users []*User) error {
	requests := make([]*msgraph.Request, len(users))
	for i, user := range users {
		body := map[string]interface{}{
			"@odata.type":     "#microsoft.graph.aadUserConversationMember",
			"roles":           []string{},
			"user@odata.bind": fmt.Sprintf("https://graph.microsoft.com/v1.0/users('%s')", user.ID),
		}
		bodyBytes, err := json.Marshal(body)
		if err != nil {
			return errors.Wrap(err, "marshal request body")
		}
		app.Logger.Debugf("request body %s", string(bodyBytes))
		requests[i] = &msgraph.Request{
			Method:   http.MethodPost,
			Resource: fmt.Sprintf("teams/%s/members", teamID),
			Headers: map[string][]string{
				"Content-Type": {"application/json"},
			},
			Body: bytes.NewBuffer(bodyBytes),
		}
	}
	responses, err := app.GraphClient.DoBatch(requests...)
	if err != nil {
		return errors.Wrap(err, "execute batch request")
	}
	for i, response := range responses {
		if response.Err != nil {
			app.Logger.Error(errors.Wrapf(err, "failed to add user %s", users[i]))
		} else {
			app.Logger.Infof("successfully added user %s", users[i])
		}
	}

	return nil
}

func fetchGroupMembers(groupID string) ([]*User, error) {
	//goland:noinspection GoPreferNilSlice
	ret := []*User{}
	if err := app.GraphClient.DoDefaultArrayRequest(&msgraph.Request{
		Method:   http.MethodGet,
		Resource: fmt.Sprintf("groups/%s/members", groupID),
		Select:   []string{"id", "userPrincipalName", "displayName"},
	}, &ret, "value"); err != nil {
		return nil, errors.Wrap(err, "default array request")
	}

	return ret, nil
}

type User struct {
	ID                string `json:"id"`
	UserPrincipalName string `json:"userPrincipalName"`
	DisplayName       string `json:"displayName"`
}

func (u User) String() string {
	return fmt.Sprintf("%s (%s; %s)", u.DisplayName, u.UserPrincipalName, u.ID)
}
