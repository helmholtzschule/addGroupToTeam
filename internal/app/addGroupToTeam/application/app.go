/*
 * Copyright (c) 2021 Julian Hackenberg. All rights reserved.
 */

package application

import (
	"log"

	"gitlab.com/helmholtzschule/addGroupToTeam/internal/pkg/application"
	"gitlab.com/helmholtzschule/addGroupToTeam/internal/pkg/config/defaults"
	"gitlab.com/helmholtzschule/addGroupToTeam/pkg/msgraph"
)

type App struct {
	application.DefaultApp
	GraphClient *msgraph.Client
}

func NewApp() *App {
	app := &App{}

	if err := app.Init(application.Manifest{
		Name:           "addGroupToTeam",
		DisplayName:    "AddGroupToTeam",
		ConfigDefaults: []map[string]interface{}{defaults.Application},
	}); err != nil {
		log.Panicln(err)
	}
	app.AddGraphClient()

	return app
}
