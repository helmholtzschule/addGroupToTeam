/*
 * Copyright (c) 2021 Julian Hackenberg. All rights reserved.
 */

package application

import (
	"context"

	"gitlab.com/helmholtzschule/addGroupToTeam/pkg/msal"
	"gitlab.com/helmholtzschule/addGroupToTeam/pkg/msgraph"
)

func (app *App) AddGraphClient() {
	app.GraphClient = msgraph.NewClient(msal.NewClient(context.Background(), app.Config.Sub("msal")))
}
