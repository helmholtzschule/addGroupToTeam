/*
 * Copyright (c) 2021 Julian Hackenberg. All rights reserved.
 */

package config

import (
	"path/filepath"

	"github.com/iancoleman/strcase"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"gitlab.com/helmholtzschule/addGroupToTeam/internal/pkg/config/defaults"
)

func LoadDefaultConfig(appName string, defaultValues ...map[string]interface{}) (*viper.Viper, error) {
	instance := NewDefaultConfig(appName)
	defaults.Set(instance, defaultValues...)
	if err := instance.ReadInConfig(); err != nil {
		return nil, errors.Wrap(err, "read config from file")
	}

	return instance, nil
}

func NewDefaultConfig(appName string) *viper.Viper {
	instance := viper.New()
	ApplyDefaultOptions(instance, appName)
	return instance
}

func ApplyDefaultOptions(instance *viper.Viper, appName string) {
	ApplyDefaultSearchOptions(instance, appName)
	ApplyDefaultEnvOptions(instance, appName)
	ApplyDefaultWatchOptions(instance)
}

func ApplyDefaultWatchOptions(instance *viper.Viper) {
	instance.WatchConfig()
}

func ApplyDefaultEnvOptions(instance *viper.Viper, appName string) {
	instance.SetEnvPrefix(strcase.ToScreamingSnake("HVH " + appName))
	instance.AutomaticEnv()
}

func ApplyDefaultSearchOptions(instance *viper.Viper, appName string) {
	appName = strcase.ToKebab(appName)
	instance.SetConfigName(appName + ".conf")
	instance.AddConfigPath(filepath.Join("/etc/hvh", appName))
	instance.AddConfigPath(filepath.Join("$HOME/.hvh", appName))
	instance.AddConfigPath(".")
}
