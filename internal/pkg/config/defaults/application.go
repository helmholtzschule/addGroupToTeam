/*
 * Copyright (c) 2021 Julian Hackenberg. All rights reserved.
 */

package defaults

var Application = map[string]interface{}{
	"logger": Logger,
}
