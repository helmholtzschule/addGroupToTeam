/*
 * Copyright (c) 2021 Julian Hackenberg. All rights reserved.
 */

package defaults

import "github.com/spf13/viper"

func Set(conf *viper.Viper, defaults ...map[string]interface{}) {
	for _, m := range defaults {
		for k, v := range m {
			conf.SetDefault(k, v)
		}
	}
}
