/*
 * Copyright (c) 2021 Julian Hackenberg. All rights reserved.
 */

package application

type Manifest struct {
	Name           string
	DisplayName    string
	ConfigDefaults []map[string]interface{}
}
