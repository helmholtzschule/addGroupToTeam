/*
 * Copyright (c) 2021 Julian Hackenberg. All rights reserved.
 */

package application

import (
	"github.com/kataras/golog"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"gitlab.com/helmholtzschule/addGroupToTeam/internal/pkg/config"
	"gitlab.com/helmholtzschule/addGroupToTeam/internal/pkg/logger"
)

type DefaultApp struct {
	Manifest Manifest
	Config   *viper.Viper
	Logger   *golog.Logger
}

func (app *DefaultApp) Init(manifest Manifest) error {
	app.Manifest = manifest

	if err := app.addConfig(); err != nil {
		return errors.Wrap(err, "add configuration")
	}
	if err := app.addLogger(); err != nil {
		return errors.Wrap(err, "add logger")
	}

	return nil
}

func (app *DefaultApp) addConfig() error {
	c, err := config.LoadDefaultConfig(app.Manifest.Name, app.Manifest.ConfigDefaults...)
	if err != nil {
		return err
	}
	app.Config = c

	return nil
}

func (app *DefaultApp) addLogger() error {
	l, err := logger.NewDefaultLogger(app.Manifest.DisplayName, app.Config.Sub("logger"))
	if err != nil {
		return err
	}
	app.Logger = l

	return nil
}
